import os

os.environ["TZ"] = "America/Fortaleza"


class BaseConfig(object):
    DEBUG = False
    JSON_AS_ASCII = False
    LOCALE = os.environ.get("LOCALE")
    SECRET_KEY = os.environ.get("SECRET_KEY")
    MONGO_URI = os.environ.get("MONGO_URI")


class development(BaseConfig):
    DEBUG = True
    DEVELOPMENT = True


class production(BaseConfig):
    DEVELOPMENT = False
