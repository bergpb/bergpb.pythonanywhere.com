import os
import locale
from unicodedata import normalize


def format_coin(coin):
    """Convert into a PT-BR currency."""
    locale.setlocale(locale.LC_MONETARY, os.environ.get("LOCALE"))
    return locale.currency(coin, grouping=True, symbol=False)


def remove_special_characters(string):
    """Remove special characters from string"""
    return normalize("NFKD", string).encode("ASCII", "ignore").decode("ASCII")


def add_underscore(string):
    """Replace space with underscore in string"""
    return string.replace(" ", "_")
