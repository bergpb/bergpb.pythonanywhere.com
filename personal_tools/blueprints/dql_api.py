from flask import Blueprint, jsonify
from personal_tools.utils import return_phrase


dql_api = Blueprint("dql_api", __name__, url_prefix="/api")


@dql_api.get("/get_washer")
def index():
    data = return_phrase()

    return jsonify(
        {"current_washer": {"phrase": data["phrase"], "date": data["date"]}}
    )
